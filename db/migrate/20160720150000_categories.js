db.categories.insert(
	[
		{
			"title" : "Teamwork",
			"icon" : "teamwork",
			"color" : "purple"
		},
		{
			"title" : "Innovation",
			"icon" : "innovation",
			"color" : "orange"
		},
		{
			"title" : "Achievement",
			"icon" : "achievement",
			"color" : "amber"
		},
		{
			"title" : "Passion",
			"icon" : "passion",
			"color" : "red-800"
		},
		{
			"title" : "Leadership",
			"icon" : "leadership",
			"color" : "cyan"
		},
		{
			"title" : "Collaboration",
			"icon" : "collaboration",
			"color" : "purple-200"
		},
		{
			"title" : "Continuous Improvement",
			"icon" : "improvement",
			"color" : "teal-900"
		},
		{
			"title" : "Agility",
			"icon" : "agility",
			"color" : "purple-400"
		},
		{
			"title" : "Determination",
			"icon" : "determination",
			"color" : "red"
		},
		{
			"title" : "Efficiency",
			"icon" : "efficiency",
			"color" : "blue"
		},
		{
			"title" : "Think Big",
			"icon" : "thinking",
			"color" : "teal"
		},
	]
);
