'use strict';

const config = require('../../config');
const nodemailer = require('nodemailer');
const sesTransport = require('nodemailer-ses-transport');
const Promise = require('bluebird');
const debug = require('debug')('MailLib');
const fs = Promise.promisifyAll(require('fs'));
const notificationFile = __dirname + '/../../views/emails/assignment/dist/index.html';
const palette = require('google-material-color');

const MailLib = {

  starNotification: function(mailFrom, authUser, toUser, category, comment) {
    debug('mailFrom');
    debug(mailFrom);

    debug('authUser');
    debug(authUser);

    debug('toUser');
    debug(toUser);

    debug('category');
    debug(category);

    debug('comment');
    debug(comment);

    return fs.readFileAsync(notificationFile, 'utf8')
      .then((fileContents) => {
        return new Promise((resolve, reject) => {
          const options = config.amazon.ses;
          const emptyUserImageURL = `${config.mail.images_url}empty-user.jpeg`;

          let colorParts = category.color.split('-');
          colorParts[0] = colorParts[0][0].toUpperCase() + colorParts[0].slice(1).toLowerCase();
          const categoryColor = palette.get(colorParts[0], colorParts[1] ? colorParts[1] : null);

          let transporter = nodemailer.createTransport(sesTransport(options));

          let templates = {
            subject: `${authUser.first_name} te ha asignado una estrella!`,
            html: fileContents
          };

          let templateVars = {
            userFirstName: authUser.first_name,
            userFromPicture: authUser.image_url ? authUser.image_url : emptyUserImageURL,
            userToPicture: toUser.image_url ? toUser.image_url : emptyUserImageURL,
            userFrom: authUser.email,
            userTo: toUser.email,
            category: category.title,
            categoryImageName: category.icon.toLowerCase(),
            categoryColor: categoryColor,
            imagesBaseUrl: config.mail.images_url,
            comment: comment
          };

          debug('templateVars');
          debug(templateVars);

          let mailOptions = {
            from: mailFrom,
            to: toUser.email
          };

          let send = transporter.templateSender(templates, mailOptions);

          return send(mailOptions, templateVars)
            .then(() => {
              debug('Message sent');

              resolve({
                'status': 'sent'
              });
            }).catch((err) => {
              reject(err);
            });
        });
      });
  }

};

module.exports = MailLib;
