'use strict';

const defaultConfig = {
  app: {
    title: 'Edrans.me',
    description: 'Edrans.me',
    keywords: 'Edrans.me'
  },
  port: process.env.PORT || 8080,
  db: 'mongodb://' + (process.env.MONGO_HOST || 'localhost') + '/starme',
  auth: {
    whitelisted_email_domains : [ 'edrans.com' ],
    google: {
      client_id: process.env.GOOGLE_CLIENT_ID,
      client_secret: process.env.GOOGLE_CLIENT_SECRET,
      redirect_uri : 'postmessage'
    }
  },
  mail: {
    from: 'gosocial@edrans.com',
    invite_subject: 'Invitation to Edrans.me',
    images_url: 'https://www.edrans.me/assets/images/emails/'
  },
  amazon: {
    ses: {
      accessKeyId: 'AKIAJYSIOQPRE7NLUY2A',
      secretAccessKey: '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'
    }
  }
};

module.exports = defaultConfig;
