'use strict';

const defaultConfig = {
  app: {
    title: 'Edrans StarME Default',
    description: 'Edrans StarME Default',
    keywords: 'Edrans StarME Default',
    version: '0.1.0'
  },
  amazon: {
    ses: {

    }
  },
  port: process.env.PORT || 8080,
  db: 'mongodb://localhost/starme',
  tokenEndPoint: '/auth/login',
  paginators : {
    assignments : {
      limit: 10
    }
  }
};

module.exports = defaultConfig;
