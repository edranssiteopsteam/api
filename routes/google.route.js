'use strict';

const GoogleController = require('../controllers/google.controller');

function routes(server) {
  var thisController = new GoogleController();

  server.get('/google/postmessage', (req, res, next) => {
    thisController.postmessage(req, res, next);
  });

  server.post('/google/postmessage', (req, res, next) => {
    thisController.postmessage(req, res, next);
  });
}

module.exports = routes;
