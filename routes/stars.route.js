'use strict';

const path = require('path');
const glob = require('glob');
const debug = require('debug')('routes');
const starsController = require('../controllers/stars.controller');
const oauth2 = require('../helpers/oauth2');

function routes(server) {
  var thisController = new starsController();

  server.get({
      url: '/categories'
    },
    oauth2.middleware,
    (req, res, next) => {
      thisController.listCategories(req, res, next);
    });

  server.post({
      url: '/assignments'
    },
    oauth2.middleware,
    (req, res, next) => {
      thisController.assignment(req, res, next);
    });

  server.get({
      url: '/assignments'
    },
    oauth2.middleware,
    (req, res, next) => {
      thisController.assignmentList(req, res, next);
    });

  /*server.post({url: '/stars/create'},
  (req, res, next) => {
    thisController.createCategory(req, res, next);
  });*/
}

module.exports = routes;
