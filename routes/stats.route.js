'use strict';

const StatsController = require('../controllers/stats.controller');
const oauth2 = require('../helpers/oauth2');

function routes(server) {
  var thisController = new StatsController();

  server.get('/stats/top', oauth2.middleware, (req, res, next) => {
    thisController.top(req, res, next);
  });

}

module.exports = routes;
