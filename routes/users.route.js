'use strict';

const UsersController = require('../controllers/users.controller');
const oauth2 = require('../helpers/oauth2');

function routes(server) {
  var thisController = new UsersController();

  server.get('/users/me', oauth2.middleware, (req, res, next) => {
    thisController.me(req, res, next);
  });

  server.get({ url: '/users/:userId',
  validation: {
    resources: {
      userId: { isRequired: true, isHexadecimal: true },
    }
  }}, (req, res, next) => {
    thisController.get(req, res, next);
  });

  server.get('/users', oauth2.middleware, (req, res, next) => {
    thisController.list(req, res, next);
  });
}

module.exports = routes;
