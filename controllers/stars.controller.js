'use strict';

const mongoose = require('mongoose');
const Controller = require('../libs/controller');
const Category = mongoose.model('Category');
const Assignment = mongoose.model('Assignment');
const restify = require('restify');
const User = mongoose.model('User');
const config = require('../config');
const MailLib = require('../libs/mail');
const debug = require('debug')('controllers:stars');
const ObjectId = mongoose.Types.ObjectId;

class StarsController extends Controller {

  listCategories(req, res, next) {
    Category.find().exec()
      .then((categories) => {
        res.send(categories);
      }).catch((err) => {
        next(err);
      });
  }

  assignment(req, res, next) {
    const body = JSON.parse(req.body.toString('utf8'));
    const attr = body.data.attributes;
    attr.category = new ObjectId(attr.category);
    // remove date sent from client, use date from server
    delete attr.date;

    if (req.user.email === attr.to) {
      return next(new restify.errors.ConflictError('Cannot give a star to yourself'));
    }

    let validDomain = false;
    config.auth.whitelisted_email_domains.map((domain) => {
      let validDomainRegExp = new RegExp(`@${domain}$`);

      if (validDomainRegExp.test(attr.to)) {
        validDomain = true;
      }
    });

    if (!validDomain) {
      return next(new restify.errors.ConflictError('Cannot give a star to external users'));
    }

    let assignment = new Assignment(attr);

    Category.findOne({
      _id: new ObjectId(attr.category)
    })
    .then((category) => {
      return assignment.save()
        .then(() => {
          return Promise.resolve({
            category: category
          });
        });
    })

    .then((result) => {
      return User.findOne({
        email: attr.from
      })
      .exec()
      .then((user) => {
        result.user = user;
        return Promise.resolve(result);
      });
    })
    .then((result) => {
      if (result.user) {
        if (result.user.given > 0) {
          return User.update({
            '_id': result.user._id
          }, {
            $inc: {
              'given': -1
            }
          }).exec()
          .then(() => {
            return Promise.resolve(result);
          });
        } else {
          throw new restify.errors.ConflictError('You dont have available stars to assign');
        }
      } else {
        return Promise.resolve(result);
      }
    })
    .then((result) => {
      return User.findOne({
        email: attr.to
      }).exec()
      .then((userTo) => {
        result.userTo = userTo;
        return Promise.resolve(result);
      });
    })
    .then((result) => {
      if (result.userTo) {
        return User.update({
          '_id': result.userTo._id
        }, {
          $inc: {
            'received': 1
          }
        }).exec()
        .then(() => {
          return Promise.resolve(result);
        });
      } else {
        return Promise.resolve(result);
      }
    })
      .then((result) => {
        const data = {
          from: req.user,
          to: result.userTo ? result.userTo : {
            email: attr.to
          }
        };

        return MailLib.starNotification(config.mail.from, data.from, data.to, result.category, attr.comment)
          .then((result) => {
            debug(result);
          });
      })
      .then(() => {
        res.send([assignment]);
      })
      .catch((err) => {
        next(err);
      }).done();
  }

  assignmentList(req, res, next) {
    const ObjectId = mongoose.Types.ObjectId;
    const paginator = {
      page: parseInt(req.query.page) || 1,
      limit: parseInt(req.query.per_page) || config.paginators.assignments.limit
    };

    let filters = {};

    if (req.params.to) filters.to = req.params.to;
    if (req.params.from) filters.from = req.params.from;
    if (req.params.category) filters.category = new ObjectId(req.params.category);

    if (req.params.startDate && req.params.endDate) {
      filters.date = {
        '$gte': new Date(req.params.startDate),
        '$lt': new Date(req.params.endDate)
      };
    } else {
      if (req.params.startDate) {
        filters.date = {
          '$gte': new Date(req.params.startDate)
        };
      }
      if (req.params.endDate) {
        filters.date = {
          '$lt': new Date(req.params.endDate)
        };
      }
    }

    Assignment.paginate(filters, Object.assign(paginator, {
      sort: {
        date: -1
      },
      populate: ['user_from', 'user_to']
    }))
      .then((assignments) => {
        let result = {
          items: [],
          meta: {
            total_pages: assignments.pages
          }
        };

        assignments.docs.forEach((item) => {
          let parsedItem = {
            'date': item.date,
            'category': item.category,
            'comment': item.comment,
            '_id': item._id,
            'from': item.from,
            'to': item.to
          };

          if (item.user_from[0]) {
            parsedItem.from = {
              'email': item.user_from[0].email,
              'first_name': item.user_from[0].first_name,
              'last_name': item.user_from[0].last_name,
              'image_url': item.user_from[0].image_url
            };
          }

          if (item.user_to[0]) {
            parsedItem.to = {
              'email': item.user_to[0].email,
              'first_name': item.user_to[0].first_name,
              'last_name': item.user_to[0].last_name,
              'image_url': item.user_to[0].image_url
            };
          }

          result.items.push(parsedItem);
        });

        res.send(result);
      }).catch((err) => {
        return next(err);
      });
  }

  /*createCategory(req, res, next){
    var data = {
      title: 'Test2',
      image_url: 'test2.test2'
    };

    var category = new Category(data);

    category.save(function(err) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(category);
      }
    });
  }*/

}

module.exports = StarsController;
