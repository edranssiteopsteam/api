'use strict';

const Controller = require('../libs/controller');
const restify = require('restify');
const mongoose = require('mongoose');
const User = mongoose.model('User');

class AuthController extends Controller {

  logout(req, res, next) {

    req.user.token = '';
    req.user.save((e) => {
      if (e) {
        return next(e);
      }

      res.status(200);
      res.send();
    });

  }
}

module.exports = AuthController;
