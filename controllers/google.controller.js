'use strict';

const Controller = require('../libs/controller');
const config = require('../config');

const restify = require('restify');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const User = mongoose.model('User');

const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const plus = google.plus('v1');
const url = require('url');

const oauthHooks = require('../helpers/oauth2/hooks');

const debug = require('debug')('controllers.google');

let oauth2Client = new OAuth2(config.auth.google.client_id, config.auth.google.client_secret, config.auth.google.redirect_uri);

class BaseController extends Controller {

  handleUserCreation(userData) {
    return new Promise((resolve, reject) => {
      let validDomain = false;

      let profilePicture = url.parse(userData.image.url);
      profilePicture.query = {};
      profilePicture.search = '';
      userData.image.url = url.format(profilePicture);

      config.auth.whitelisted_email_domains.map((domain) => {
        let validDomainRegExp = new RegExp(`@${domain}$`);

        if (validDomainRegExp.test(userData.emails[0].value)) {
          validDomain = true;
        }
      });

      debug(`Valid domain: ${validDomain}`);

      if (!validDomain) {
        return reject(new restify.errors.UnauthorizedError('Invalid email domain'));
      }

      return User.findOne({email: userData.emails[0].value}).exec()
      .then((dbUser) => {
        if (dbUser) {
          if (!dbUser.first_name) {
            dbUser.first_name = userData.name.givenName;
          }

          if (!dbUser.last_name) {
            dbUser.last_name = userData.name.familyName;
          }

          if (!dbUser.image_url) {
            dbUser.image_url = userData.image && userData.image.url ? userData.image.url : '';
          }

          let dataForToken = `${dbUser.email}:${dbUser.etag}`;
          let userToken = oauthHooks.generateToken(dataForToken);

          dbUser.token = userToken;
          debug(`Generated token: ${userToken}`);

          return { user: dbUser, token: userToken };
        } else {
          const newUserData = {
            email: userData.emails[0].value,
            first_name : userData.name.givenName,
            last_name : userData.name.familyName,
            image_url : userData.image && userData.image.url ? userData.image.url : ''
          };

          let newUser = new User(newUserData);
          let dataForToken = `${newUser.email}:${newUser._id}`;
          let userToken = oauthHooks.generateToken(dataForToken);
          newUser.token = userToken;
          debug(`Generated token: ${userToken}`);

          return { user: newUser, token: userToken };
        }
      })
      .then((data) => {
        data.user.save(() => {
          resolve(data.token);
        });
      });
    });
  }

  postmessage(req, res, next) {
    debug(req.body.code);

    oauth2Client.getToken(req.body.code, (err, tokens) => {
      debug(err);
      debug(tokens);

      if (!err) {
        oauth2Client.setCredentials(tokens);

        plus.people.get({userId: 'me', auth: oauth2Client}, (err, response) => {
          debug(err);
          debug(response);

          if (err) {
            return next(new restify.errors.ServiceUnavailableError('Service Unavailable'));
          }

          this.handleUserCreation(response).then((result) => {
            res.send({access_token: result, token_type: 'Bearer'});
          }).catch((err) => {
            next(err);
          });
        });
      }
    });
  }

}

module.exports = BaseController;
