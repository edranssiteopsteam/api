'use strict';

const mongoose = require('mongoose');
const Controller = require('../libs/controller');
const StatMonth = mongoose.model('StatMonth');
const moment = require('moment');

class StatsController extends Controller {

  top(req, res, next) {
    const availableTypes = [ 'received', 'given' ];
    const thisMonth = moment().utc().format('YYYY-MM');

    let sort = {
      received: -1
    };

    let limit = (req.query.limit ? parseInt(req.query.limit) : 5);

    if (req.query.type && availableTypes.indexOf(req.query.type) > -1) {
      sort = {};
      sort[req.query.type] = -1;
    }

    StatMonth.find({month: thisMonth }).sort(sort).limit(limit).exec()
    .then((results) => {
      res.send(results);
    }).catch((err) => {
      next(err);
    });
  }

}

module.exports = StatsController;
