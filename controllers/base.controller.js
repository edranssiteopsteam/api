'use strict';

const Controller = require('../libs/controller');

class BaseController extends Controller {

  handler(req, res) {
    res.send({
      status: 'ok'
    });
  }

  secureHandler(req, res) {
    res.send({
      status: 'secure_ok'
    });
  }
}

module.exports = BaseController;
