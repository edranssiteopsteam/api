'use strict';

const path = require('path');
const glob = require('glob');
const debug = require('debug')('controllers');
const Controller = require('../libs/controller');
const oauth2 = require('../helpers/oauth2');
const restify = require('restify');
const mongoose = require('mongoose');
const User = mongoose.model('User');

class UsersController extends Controller {

  handler(req, res, next) {
    res.send({controller: 'users'});
  }

  me(req, res, next) {
    res.status(200);
    res.send(req.user);
  }

  get(req, res, next) {
    if (req.params.userId.length !== 24) {
      return next(new restify.errors.InvalidArgumentError('Invalid user id'));
    }

    User.findOne({_id: req.params.userId})
    .then((user) => {
      if (user) {
        user.salt = undefined;
        user.password = undefined;

        res.send(user);
      } else {
        return next(new restify.errors.ResourceNotFoundError('User not found'));
      }
    }).catch((err) => {
      next(err);
    });
  }

  list(req, res, next) {
    User.find({})
    .then((users) => {
      users.map((user) => {
        user.salt = undefined;
        user.password = undefined;
      });

      res.send(users);
    }).catch((err) => {
      next(err);
    });
  }
}

module.exports = UsersController;
