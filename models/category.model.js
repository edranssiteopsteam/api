'use strict';

const debug = require('debug')('models');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let categorySchema = new Schema({
  title:  String,
  icon:  String,
  color: String
});

mongoose.model('Category', categorySchema);
