'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let StatMonth = new Schema({
  month: {
    type: String
  },
  email : String,
  category: {
    type: Schema.ObjectId,
    ref: 'Category'
  },
  given: {
    type: Number,
    default: 0
  },
  received: {
    type: Number,
    default: 0
  }
});

mongoose.model('StatMonth', StatMonth);
