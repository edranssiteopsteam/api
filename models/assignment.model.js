'use strict';

const debug = require('debug')('models');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

let AssignmentSchema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  from: String,
  to: String,
  category: {
    type: Schema.ObjectId,
    ref: 'Category'
  },
  comment: String
});

AssignmentSchema.virtual('user_from', {
  ref: 'User',
  localField: 'from',
  foreignField: 'email'
});

AssignmentSchema.virtual('user_to', {
  ref: 'User',
  localField: 'to',
  foreignField: 'email'
});

AssignmentSchema.plugin(mongoosePaginate);

mongoose.model('Assignment', AssignmentSchema);
